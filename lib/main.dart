import 'dart:io';

//import 'package:archive/archive_io.dart';
import 'package:archive/archive.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:path_provider/path_provider.dart';
import 'package:testebaixarflipbookwebview/view.dart';

void main() async {
    WidgetsFlutterBinding.ensureInitialized();

    SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft,DeviceOrientation.landscapeRight]);

    if (Platform.isAndroid) {
      await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const AppWEBview(),
    );
  }
}

class AppWEBview extends StatefulWidget {
  const AppWEBview({Key? key}) : super(key: key);

  @override
  State<AppWEBview> createState() => _AppWEBviewState();
}

class _AppWEBviewState extends State<AppWEBview> {
  late bool _downloading;
  late String _dir;
  late Uri _dirtwo;
  final String _zipPath = 'https://luanfern.mxw.com.br/download/flipbook.zip';
  final String _localZipFileName = 'flipbook.zip';

  @override
  void initState() {
    super.initState();
    _downloading = false;
    _initDir();
  }

  _initDir() async {
    _dir = (await getApplicationDocumentsDirectory()).path;
    _dirtwo = (await getApplicationDocumentsDirectory()).uri;
    print(_dirtwo);
  }

  Future<File> _downloadFile(String url, String fileName) async {
    final req = await Dio().get(
      url,
      options: Options(
        responseType: ResponseType.bytes,
        followRedirects: false,
        receiveTimeout: 0,
      ),
    );
    var file = File('$_dir/$fileName');
    return file.writeAsBytes(req.data);
  }

  Future<void> _downloadZip() async {
    setState(() {
      _downloading = true;
    });

    var zippedFile = await _downloadFile(_zipPath, _localZipFileName);
    await unarchiveAndSave(zippedFile);

    setState(() {
      _downloading = false;
      print('trocando de tela');
      print(_dirtwo.toFilePath());
    });
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => View(
            cam: _dirtwo.toString(),
          ),
        ),
      );
  }

  unarchiveAndSave(var zippedFile) async {
    var bytes = zippedFile.readAsBytesSync();
    var archive = ZipDecoder().decodeBytes(bytes);
    for (var file in archive) {
      var fileName = '$_dir/${file.name}';
      if (file.isFile) {
        var outFile = File(fileName);
        print('File:: ' + outFile.path);
        outFile = await outFile.create(recursive: true);
        await outFile.writeAsBytes(file.content);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.amberAccent,
        body: !_downloading
            ? Center(
                child: ElevatedButton(
                  onPressed: () {
                    _downloadZip();
                  },
                  child: const Text('Baixar e mostrar'),
                ),
              )
            : const Center(
                child: CircularProgressIndicator(),
              ));
  }
}
